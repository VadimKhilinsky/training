﻿namespace Task1_1
{
    public enum LinkType
    {
        Unknown,
        Html,
        Image,
        Audio,
        Video
    }
}