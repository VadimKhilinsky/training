﻿using System;

namespace Task1_1
{
    public static class IdGenerator
    {
        public static void NewId(this Entity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            entity.Id = Guid.NewGuid();
        }
    }
}