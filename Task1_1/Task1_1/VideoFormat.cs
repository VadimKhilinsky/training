﻿namespace Task1_1
{
    public enum VideoFormat
    {
        Unknown,
        Avi,
        Mp4,
        Flv
    }
}