﻿using System;

namespace Task1_1
{
    public class NetworkResourceLink : TrainingMaterial, ICloneable
    {
        private string _contentUri;

        public NetworkResourceLink(string description, string contentUri, LinkType type) : base(description)
        {
            ContentUri = contentUri;
            Type = type;
        }

        public NetworkResourceLink(string contentUri, LinkType type)
        {
            ContentUri = contentUri;
            Type = type;
        }

        public string ContentUri
        {
            get => _contentUri;
            set => _contentUri = string.IsNullOrEmpty(value)
                ? throw new ArgumentNullException(nameof(_contentUri))
                : value;
        }

        public LinkType Type { get; set; }

        public override object Clone()
        {
            return new NetworkResourceLink(ContentUri, Type);
        }
    }
}