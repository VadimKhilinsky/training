﻿using System;

namespace Task1_1
{
    public class TextMaterial : TrainingMaterial, ICloneable
    {
        private string _text;

        public TextMaterial(string description, string text) : base(description)
        {
            Text = text;
        }

        public TextMaterial(string text):base("")
        {
            Text = text;
        }

        public string Text
        {
            get => _text;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException(nameof(value));
                }
                if (value.Length > 10000)
                {
                    throw new ArgumentException("length should be less 10000", nameof(value));

                }

                _text = value;
            } 
        }

        public override object Clone()
        {
            return new TextMaterial(Description,Text);
        }
    }
}