﻿namespace Task1_1
{
    public interface IVersionable
    {
        byte[] GetVersion();
        void SetVersion(byte[] version);
    }
}