﻿using System;

namespace Task1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var lesson = new TrainingLesson();
                lesson.NewId();
                lesson.SetVersion(new byte[] {4, 2, 4, 6, 12, 34, 65, 77});

                var video1 = new VideoMaterial("someContent", "someScreen", VideoFormat.Mp4);
                video1.NewId();
                video1.SetVersion(new byte[] {1, 2, 3, 6, 15, 16, 17, 45});
                lesson.Add(video1);

                var networkResource = new NetworkResourceLink("someContent", LinkType.Audio);
                networkResource.NewId();
                lesson.Add(networkResource);

                var text = new TextMaterial("text text");
                text.NewId();
                lesson.Add(text);

                Console.WriteLine($"lesson type: {lesson.GetLessonType()}");

                Console.WriteLine(lesson.Id);
                var copyLesson = (TrainingLesson) lesson.Clone();

                Console.WriteLine(
                    $"clone of lesson and lesson equals = {copyLesson.Equals(lesson)} (hsc1 == hsc2) = {copyLesson.GetHashCode() == lesson.GetHashCode()}");

                copyLesson.NewId();
                Console.WriteLine(copyLesson.Id);
                Console.WriteLine(lesson.Id);

            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("program was successfully finished");
                Console.ReadLine();
            }
        }
    }
}