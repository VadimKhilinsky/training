﻿using System;

namespace Task1_1
{
    public abstract class TrainingMaterial : Entity, ICloneable
    {
        public TrainingMaterial(string description = ""):base(description)
        {
        }

        public virtual object Clone()
        {
            return this;
        }
    }
}