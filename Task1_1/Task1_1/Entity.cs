﻿using System;

namespace Task1_1
{
    public abstract class Entity
    {
        private string _description;

        public string Description
        {
            get => _description;
            set
            {
                if (value?.Length > 256)
                {
                    throw new ArgumentOutOfRangeException(nameof(value),
                        "length should be less 256");
                }

                _description = value;
            }
        }

        protected Entity(string description = "")
        {
            Description = description;
        }

        public Guid Id { get; set; }

        public override string ToString()
        {
            return Description;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var entity = obj as Entity;

            if (entity == null)
            {
                return false;
            }

            return Id == entity.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}