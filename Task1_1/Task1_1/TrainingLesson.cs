﻿using System;

namespace Task1_1
{
    public class TrainingLesson : Entity, IVersionable, ICloneable
    {
        private int _capacity;
        private int _count;
        private TrainingMaterial[] _trainingMaterials;
        private readonly byte[] _version = new byte[8];

        public TrainingLesson(string description = "",int capacity = 4):base(description)
        {
            if (capacity >= 0)
            {
                _capacity = capacity;
                _trainingMaterials = new TrainingMaterial[_capacity];
            }
            else
            {
                throw new ArgumentException("shouldn't be negative", nameof(capacity));
            }
        }

        public object Clone()
        {
            var lesson = new TrainingLesson(Description,_capacity) {_count = _count};
            for (int i = 0; i < _count; i++)
            {
                lesson._trainingMaterials[i] = (TrainingMaterial)_trainingMaterials[i].Clone();
            }
            lesson.SetVersion(GetVersion());
            lesson.Id = Id;

            return lesson;
        }

        public byte[] GetVersion()
        {
            var version = new byte[8];
            Array.Copy(version, _version, 8);

            return version;
        }

        public void SetVersion(byte[] version)
        {
            if (version == null)
            {
                throw new ArgumentNullException(nameof(version));
            }

            if (version.Length != 8)
            {
                throw new ArgumentException("should consist of 8 bytes", nameof(version));
            }

            Array.Copy(version, _version, 8);
        }

        public void Add(TrainingMaterial material)
        {
            if (material == null)
            {
                return;
            }

            if (_capacity <= _count + 1)
            {
                _capacity *= 2;
                Array.Resize(ref _trainingMaterials, _capacity);
            }

            _trainingMaterials[_count++] = material;
        }

        public LessonType GetLessonType()
        {
            foreach (var material in _trainingMaterials)
            {
                if (material is VideoMaterial)
                {
                    return LessonType.VideoLesson;
                }
            }

            return LessonType.TextLesson;
        }
    }
}