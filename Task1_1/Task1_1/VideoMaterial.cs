﻿using System;
using System.Security.Cryptography;

namespace Task1_1
{
    public class VideoMaterial : TrainingMaterial, IVersionable, ICloneable
    {
        private readonly byte[] _version = new byte[8];
        private string _videoContentUri;

        public VideoMaterial(string description, string videoContentUri, string splashScreenUri,
            VideoFormat format) : base(description)
        {
            VideoContentUri = videoContentUri;
            SplashScreenUri = splashScreenUri;
            Format = format;
        }

        public VideoMaterial(string videoContentUri, string splashScreenUri, VideoFormat format)
        {
            VideoContentUri = videoContentUri;
            SplashScreenUri = splashScreenUri;
            Format = format;
        }

        public string VideoContentUri
        {
            get => _videoContentUri;
            set => _videoContentUri = string.IsNullOrEmpty(value)
                ? throw new ArgumentNullException(nameof(_videoContentUri))
                : value;
        }

        public string SplashScreenUri { get; set; }

        public VideoFormat Format { get; set; }

        public byte[] GetVersion()
        {
            return _version;
        }

        public void SetVersion(byte[] version)
        {
            if (version.Length != 8)
                throw new Exception("version should consist of 8 bytes");

            Array.Copy(version, _version, 8);
        }

        public override object Clone()
        {
            return new VideoMaterial(Description,VideoContentUri,SplashScreenUri,Format);
        }
    }
}