﻿using System;
using Listener;

namespace TextListener
{
    public class TextConfigurations : ListenerConfigurations
    {
        private string _fileName;
        private string _filePath;

        public TextConfigurations(string filePath, string fileName)
        {
            FilePath = filePath;
            FileName = fileName;
        }

        public TextConfigurations() : this(" ", " ")
        {
        }

        public string FilePath
        {
            get => _filePath;
            set => _filePath = value ?? throw new ArgumentNullException(nameof(value));
        }

        public string FileName
        {
            get => _fileName;
            set => _fileName = value ?? throw new ArgumentNullException(nameof(value));
        }
    }
}