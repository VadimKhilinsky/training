﻿using System;
using System.IO;
using System.Text;
using Listener;

namespace TextListener
{
    public class TextListener : IListener
    {
        public TextListener(TextConfigurations configs)
        {
            if (configs == null)
            {
                throw new ArgumentNullException(nameof(configs));
            }

            FilePath = configs.FilePath;
            FileName = configs.FileName;
        }

        public string FilePath { get; set; }
        public string FileName { get; set; }

        public void WriteMessage(string message, string logLevel)
        {

            if (!File.Exists($@"{Path.GetFullPath(FilePath)}{FileName}"))
            {
                using (File.Create($@"{System.IO.Path.GetFullPath(FilePath)}{FileName}"))
                {
                }
            }
            using (StreamWriter sw = new StreamWriter($@"{System.IO.Path.GetFullPath(FilePath)}{FileName}", true, Encoding.Default))
            {
                sw.WriteLine($"{logLevel}. {DateTime.Now}: {message}");
            }
        }
    }
}