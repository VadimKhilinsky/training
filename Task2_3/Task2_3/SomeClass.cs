﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tracking;

namespace Task2_3
{
    [TrackingEntity]
    public class SomeClass
    {
        [TrackingProperty("property")]
        public int SomeProperty { get; set; }
        
        [TrackingProperty("field")]
        private string _someField;

        public SomeClass(string someField)
        {
            _someField = someField;
        }
    }
}
