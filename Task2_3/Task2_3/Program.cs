﻿using System;
using Logger;

namespace Task2_3
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var logger = new Logger.Logger();
            logger.Debug("message");
            logger.Info("another message");

            logger.Track(new SomeClass("abc") {SomeProperty = 1}, LogLevel.Info);

            Console.ReadLine();
        }
        
    }
}
