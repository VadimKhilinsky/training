﻿using Listener;

namespace EventLogListener
{
    public class EventLogConfigurations : ListenerConfigurations
    {
        public EventLogConfigurations(string eventId, string category)
        {
            EventId = eventId;
            Category = category;
        }

        public EventLogConfigurations() : this("0", "0") { }

        public string EventId { get; set; }
        public string Category { get; set; }
    }
}