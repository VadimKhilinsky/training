﻿using System;
using System.Diagnostics;
using Listener;

namespace EventLogListener
{
    public class EventLogListener : IListener
    {
        public string EventId { get; set; }
        public string Category { get; set; }
        public EventLogListener(EventLogConfigurations configs)
        {
            if (configs == null)
            {
                throw new ArgumentNullException(nameof(configs));
            }

            EventId = configs.EventId;
            Category = configs.Category;
        }

        public void WriteMessage(string message, string logLevel)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            using (var eventLog = new EventLog("Application"))
            {
                eventLog.Source = "Application";

                void WriteEntryWithLog(EventLogEntryType logType)
                {
                    eventLog.WriteEntry(message, logType, int.Parse(EventId), short.Parse(Category));
                }

                switch (logLevel)
                {
                    case "Debug":
                        WriteEntryWithLog(EventLogEntryType.SuccessAudit);
                        break;
                    case "Info":
                        WriteEntryWithLog(EventLogEntryType.Information);
                        break;
                    case "Warn":
                        WriteEntryWithLog(EventLogEntryType.Warning);
                        break;
                    case "Error":
                        WriteEntryWithLog(EventLogEntryType.Error);
                        break;
                    case "Fatal":
                        WriteEntryWithLog(EventLogEntryType.FailureAudit);
                        break;
                }
            }
        }
    }
}