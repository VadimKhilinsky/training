﻿using System;

namespace Tracking
{
    [AttributeUsage(AttributeTargets.Field|AttributeTargets.Property)]
    public class TrackingPropertyAttribute : Attribute
    {
        public string Name { get; set; }

        public TrackingPropertyAttribute(string name)
        {
            Name = name;
        }

        public TrackingPropertyAttribute()
        {
        }
    }
}
