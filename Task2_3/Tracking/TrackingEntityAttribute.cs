﻿using System;

namespace Tracking
{
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Struct)]
    public class TrackingEntityAttribute : Attribute
    {
    }
}