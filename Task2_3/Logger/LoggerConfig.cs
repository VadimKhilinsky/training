﻿using System.Configuration;

namespace Logger
{
    [ConfigurationCollection(typeof(Listener), AddItemName = "listener")]
    public class ListenerCollection : ConfigurationElementCollection
    {
        public Listener this[int i] => (Listener)BaseGet(i);

        protected override ConfigurationElement CreateNewElement()
        {
            return new Listener();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Listener)element).Type;
        }
    }

    public class LoggerSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsDefaultCollection = true)]
        public ListenerCollection ListenerCollection => (ListenerCollection)base[""];

        [ConfigurationProperty("minLevel", IsRequired = true)]
        public string MinLogLevel => (string)this["minLevel"];
    }

    public class Listener : ConfigurationElement
    {
        [ConfigurationProperty("type", IsRequired = true)]
        public string Type => (string)this["type"];

        [ConfigurationProperty("values", IsRequired = true)]
        public string Values => (string)this["values"];

        [ConfigurationProperty("properties", IsRequired = false)]
        public PropertyCollection PropertyCollection => (PropertyCollection)base["properties"];
    }

    [ConfigurationCollection(typeof(Property), AddItemName = "property")]
    public class PropertyCollection : ConfigurationElementCollection
    {
        public Property this[int i] => (Property)BaseGet(i);

        protected override ConfigurationElement CreateNewElement()
        {
            return new Property();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Property) element).Name;
        }
    }

    public class Property : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name => (string)this["name"];

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value => (string)this["value"];
    }
}
