﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using Listener;
using Tracking;

namespace Logger
{
    public class Logger
    {
        private readonly List<IListener> _logListeners = new List<IListener>();

        public LogLevel MinLevel { get; set; }

        public Logger()
        {
            var section = ConfigurationManager.GetSection("logger");

            if (section == null)
            {
                throw new ArgumentNullException(nameof(section));
            }

            LoggerSection config = (LoggerSection)section;
            MinLevel = (LogLevel)Enum.Parse(typeof(LogLevel), config.MinLogLevel);
            ListenerCollection listenerCollection = config.ListenerCollection;
            foreach (Listener listener in listenerCollection)
            {
                var typeName = listener.Type.Split(',');
                var assembly = Assembly.Load(typeName[1]);
                var listenerType = assembly.GetType(typeName[0]);
                var valuesTypeName = listener.Values.Split(',');
                var valuesAssembly = Assembly.Load(valuesTypeName[1]);
                var listenerValuesType = valuesAssembly.GetType(valuesTypeName[0]);
                if (listenerValuesType == null) continue;
                var logListenerArgs = (ListenerConfigurations)Activator.CreateInstance(listenerValuesType);
                var listenerProperties = listener.PropertyCollection;
                foreach (Property listenerProperty in listenerProperties)
                {
                    var propertyInfo = listenerValuesType.GetProperty(listenerProperty.Name);
                    if (propertyInfo != null)
                    {
                        propertyInfo.SetValue(logListenerArgs, listenerProperty.Value);
                    }
                }
                if (listenerType == null) continue;
                var logListener = (IListener)Activator.CreateInstance(listenerType, args: logListenerArgs);
                _logListeners.Add(logListener);
            }
        }

        public void Trace(string message)
        {
            WriteMessage(message, LogLevel.Trace);
        }

        public void Debug(string message)
        {
            WriteMessage(message, LogLevel.Debug);
        }

        public void Info(string message)
        {
            WriteMessage(message, LogLevel.Info);
        }

        public void Warn(string message)
        {
            WriteMessage(message, LogLevel.Warn);
        }

        public void Error(string message)
        {
            WriteMessage(message, LogLevel.Error);
        }

        public void Fatal(string message)
        {
            WriteMessage(message, LogLevel.Fatal);
        }

        private void WriteMessage(string message, LogLevel logLevel)
        {
            if (logLevel < MinLevel)
            {
                throw new ArgumentOutOfRangeException(nameof(logLevel));
            }

            foreach (IListener logListener in _logListeners)
            {
                logListener.WriteMessage(message, logLevel.ToString());
            }
        }

        public void Track(object obj, LogLevel level)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            IEnumerable<(object,object)> properties = null;
            IEnumerable<(object,object)> fields = null;

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static |
                                 BindingFlags.Instance;

            if (obj.GetType().GetCustomAttribute<TrackingEntityAttribute>() != null)
            {
                properties = obj.GetType().GetProperties(flags)
                    .Where(z => z.GetCustomAttribute<TrackingPropertyAttribute>() != null)
                    .Select(x => ((object)x.Name, x.GetValue(obj)));
                fields = obj.GetType().GetFields(flags)
                    .Where(z => z.GetCustomAttribute<TrackingPropertyAttribute>() != null)
                    .Select(x => ((object)x.Name, x.GetValue(obj)));
            }

            var pairs = properties?.Concat(fields);

            var sb = new StringBuilder();

            foreach (var pair in pairs)
            {
                sb.Append($"name = {pair.Item1} value = {pair.Item2}, ");
            }

            WriteMessage(sb.ToString(), level);
        }
    }
}
