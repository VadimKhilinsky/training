﻿using System;
using Listener;

namespace WordListener
{
    public class WordConfigurations : ListenerConfigurations
    {
        private string _fileName;
        private string _filePath;
        private string _font;
        private string _fontSize;

        public WordConfigurations(string filePath, string fileName, string font = "Times New Roman", string fontSize = "10")
        {
            FilePath = filePath;
            FileName = fileName;
            Font = font;
            FontSize = fontSize;
        }

        public WordConfigurations() : this("", "", "Times New Roman", "0")
        {
        }

        public string FilePath
        {
            get => _filePath;
            set => _filePath = value ?? throw new ArgumentNullException(nameof(value));
        }

        public string FileName
        {
            get => _fileName;
            set => _fileName = value ?? throw new ArgumentNullException(nameof(value));
        }

        public string Font
        {
            get => _font;
            set => _font = value ?? throw new ArgumentNullException(nameof(value));
        }

        public string FontSize
        {
            get => _fontSize;
            set => _fontSize = value ?? throw new ArgumentNullException(nameof(value));
        }
    }
}