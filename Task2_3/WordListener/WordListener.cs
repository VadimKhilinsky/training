﻿using System;
using System.IO;
using Listener;
using Microsoft.Office.Interop.Word;

namespace WordListener
{
    public class WordListener : IListener
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string Font { get; set; }
        public string FontSize { get; set; }

        public WordListener(WordConfigurations configs)
        {
            if (configs == null)
            {
                throw new ArgumentNullException(nameof(configs));
            }

            FilePath = configs.FilePath;
            FileName = configs.FileName;
            Font = configs.Font;
            FontSize = configs.FontSize;
        }

        public void WriteMessage(string message, string logLevel)
        {
            var app = new Application();

            if (!File.Exists($@"{Path.GetFullPath(FilePath)}{FileName}"))
            {
                var saveDoc = app.Documents.Add();
                saveDoc.SaveAs($@"{Path.GetFullPath(FilePath)}{FileName}");
            }

            var doc = app.Documents.Open($@"{Path.GetFullPath(FilePath)}{FileName}");
            var par = doc.Paragraphs.Add();

            par.Range.Text = $"{logLevel}. {DateTime.Now}: {message}";
            par.Range.Font.Name = Font;
            par.Range.Font.Size = int.Parse(FontSize);
            doc.Paragraphs.Add();

            doc.Save();
            doc.Close();
            app.Quit();
        }
    }
}