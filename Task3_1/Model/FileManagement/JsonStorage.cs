﻿namespace Model.FileManagement
{
    public class JsonStorage : Storage
    {
        public override IFileManager CreateFileManager()
        {
            return new JsonFileManager();
        }
    }
}