﻿namespace Model.FileManagement
{
    public abstract class Storage
    {
        public abstract IFileManager CreateFileManager();
    }
}