﻿using System.Collections.Generic;

namespace Model.FileManagement
{
    public interface IFileManager
    {
        IEnumerable<Sensor> Read(string path);
        void Save(string path, IEnumerable<Sensor> sensors);
    }
}