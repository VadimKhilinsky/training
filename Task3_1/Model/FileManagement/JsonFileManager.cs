﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Model.FileManagement
{
    public class JsonFileManager : IFileManager
    {
        public IEnumerable<Sensor> Read(string path)
        {
            return JsonConvert.DeserializeObject<List<Sensor>>(File.ReadAllText(path));
        }

        public void Save(string path, IEnumerable<Sensor> sensors)
        {
            File.WriteAllText(path,JsonConvert.SerializeObject(sensors, Newtonsoft.Json.Formatting.Indented));
        }
    }
}