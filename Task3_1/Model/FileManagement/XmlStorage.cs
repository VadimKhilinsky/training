﻿namespace Model.FileManagement
{
    public class XmlStorage : Storage
    {
        public override IFileManager CreateFileManager()
        {
            return new XmlFileManager();
        }
    }
}