﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Model.FileManagement
{
    public class XmlFileManager : IFileManager
    {
        public IEnumerable<Sensor> Read(string path)
        {
            var xml = XDocument.Load(path);

            if (xml == null) throw new ArgumentException("path cannot be found");

            var sensors = xml.Element("config")?.Elements("sensor").Select(sensor => new Sensor
                {
                    Type = (SensorType) Enum.Parse(typeof(SensorType),
                        sensor.Element("type")?.Value ?? throw new InvalidOperationException()),
                    Interval = int.Parse(sensor.Element("interval")?.Value ?? throw new InvalidOperationException()),
                    Id = sensor.Element("id")?.Value ?? throw new InvalidOperationException()
                });

            return sensors;
        }

        public void Save(string path, IEnumerable<Sensor> sensors)
        {
            var config = new XElement("config");

            foreach (var sensor in sensors)
                config.Add(new XElement("sensor", new XElement("id", sensor.Id),
                    new XElement("interval", sensor.Interval), new XElement("type", sensor.Type)));

            var doc = new XDocument(config);

            doc.Save(path);
        }
    }
}