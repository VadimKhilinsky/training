﻿namespace Model
{
    public interface ISensorUpdate
    {
        void Update(Sensor sensor);
    }
}