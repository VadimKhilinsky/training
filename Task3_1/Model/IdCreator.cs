﻿using System;

namespace Model
{
    public class IdCreator
    {
        private static IdCreator _instance;
        private static readonly object Locker = new object();

        private IdCreator()
        {
        }

        public static IdCreator GetInstance()
        {
            lock (Locker)
            {
                return _instance ?? (_instance = new IdCreator());
            }
        }

        public string Create()
        {
            return Guid.NewGuid().ToString();
        }
    }
}