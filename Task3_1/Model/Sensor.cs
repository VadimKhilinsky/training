﻿using System;
using System.Collections.Generic;
using Model.Mods;

namespace Model
{
    public class Sensor : IObservable
    {
        private string _id;
        private double _measuredValue;
        private int _interval;
        private readonly List<ISensorUpdate> _observers = new List<ISensorUpdate>();

        public double MeasuredValue
        {
            get => _measuredValue;
            set
            {
                _measuredValue = value;
                Notify();
            }
        }

        public int Interval
        {
            get => _interval;
            set => _interval = value > 0 ? value : throw new ArgumentException(nameof(value));
        }

        public string Id
        {
            get => _id ?? IdCreator.GetInstance().Create();
            set => _id = value??throw new ArgumentNullException(nameof(value));
        }

        public SensorType Type { get; set; }

        public Mode SensorMode { get; set; }

        public Sensor()
        {
            SensorMode = new IdleMode(this);
            Id = IdCreator.GetInstance().Create();
        }

        public Sensor(Mode mode)
        {
            SensorMode = mode??throw new ArgumentNullException(nameof(mode));
            Id = IdCreator.GetInstance().Create();
        }

        public void SwitchMode()
        {
            SensorMode.SwitchMode(this);
        }

        public void Attach(ISensorUpdate observer)
        {
            if (observer != null)
            {
                _observers.Add(observer);
            }
        }

        public void Detach(ISensorUpdate observer)
        {
            _observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (var observer in _observers)
            {
                observer.Update(this);
            }
        }
    }
}
