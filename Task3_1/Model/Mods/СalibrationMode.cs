﻿using System.Threading;

namespace Model.Mods
{
    public class СalibrationMode : Mode
    {
        private Timer _timer;
        private TimerCallback _timerCallback;

        public СalibrationMode(Sensor sensor)
        {
            _timerCallback = Increment;
            _timer = new Timer(_timerCallback, sensor, 0, 1000);
        }

        public override void SwitchMode(Sensor sensor)
        {
            _timer.Dispose();

            sensor.SensorMode = new WorkMode(sensor);
        }

        public void Increment(object sensor)
        {
            ((Sensor) sensor).MeasuredValue++;
        }
    }
}