﻿using System;
using System.Threading;

namespace Model.Mods
{
    public class WorkMode : Mode
    {
        private Timer _timer;
        private TimerCallback _timerCallback;
        private readonly Random _rand = new Random();

        public WorkMode(Sensor sensor)
        {
            _timerCallback = SetRandomValue;
            _timer = new Timer(_timerCallback, sensor, 0, sensor.Interval);
        }

        public override void SwitchMode(Sensor sensor)
        {
            _timer.Dispose();

            sensor.SensorMode = new IdleMode(sensor);
        }

        private void SetRandomValue(object sensor)
        {
            ((Sensor) sensor).MeasuredValue = Convert.ToDouble(_rand.Next(1000)) / 10;
        }
    }
}