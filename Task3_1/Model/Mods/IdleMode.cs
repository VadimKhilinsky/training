﻿namespace Model.Mods
{
    public class IdleMode : Mode
    {
        public IdleMode(Sensor sensor)
        { 
        }

        public override void SwitchMode(Sensor sensor)
        {
            sensor.SensorMode = new СalibrationMode(sensor);
        }
    }
}