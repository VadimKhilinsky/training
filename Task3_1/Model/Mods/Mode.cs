﻿namespace Model.Mods
{
    public abstract class Mode
    {
        public abstract void SwitchMode(Sensor sensor);
    }
}