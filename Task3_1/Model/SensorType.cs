﻿namespace Model
{
    public enum SensorType
    {
        Default,
        Pressure,
        Temperature,
        MagneticField
    }
}