﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public interface IObservable
    {
        void Attach(ISensorUpdate observer);
        void Detach(ISensorUpdate observer);
        void Notify();
    }
}
