﻿using System.Windows;
using Microsoft.Win32;

namespace ApplicationModel.FileServices
{
    public class DefaultFileService
    {
        private const string Filter =
            "XML files (*.xml)|*.xml|All files (*.*)|*.*|JSON files (*.json)|*.json|All files (*.*)|*.*";

        private readonly OpenFileDialog _openFileDialog = new OpenFileDialog();
        private readonly SaveFileDialog _saveFileDialog = new SaveFileDialog();

        public DefaultFileService()
        {
            _openFileDialog.Filter = Filter;
            _saveFileDialog.Filter = Filter;
        }

        public string Path { get; set; }

        public void OpenFileDialog()
        {
            if (_openFileDialog.ShowDialog() == true)
            {
                Path = _openFileDialog.FileName;
            }
        }

        public void SaveFileDialog()
        {
            if (_saveFileDialog.ShowDialog() == true)
            {
                Path = _saveFileDialog.FileName;
            }
        }

        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}