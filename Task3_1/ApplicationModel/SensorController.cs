﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Model;
using Model.Mods;

namespace ApplicationModel
{
    public class SensorController : ISensorUpdate, INotifyPropertyChanged
    {
        public SensorController(Sensor controlledSensor)
        {
            ControlledSensor = controlledSensor;
            ControlledSensor.SensorMode = new IdleMode(ControlledSensor);
            ControlledSensor.Attach(this);
            Id = IdCreator.GetInstance().Create();
        }

        public Sensor ControlledSensor { get; set; }

        public string Id
        {
            get => ControlledSensor.Id;
            set
            {
                ControlledSensor.Id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        public double Value
        {
            get => ControlledSensor.MeasuredValue;
            set => ControlledSensor.MeasuredValue = value;
        }

        public int Interval
        {
            get => ControlledSensor.Interval;
            set
            {
                ControlledSensor.Interval = value;
                OnPropertyChanged(nameof(Interval));
            }
        }

        public SensorType Type
        {
            get => ControlledSensor.Type;
            set
            {
                ControlledSensor.Type = value;
                OnPropertyChanged(nameof(Type));
            }
        }

        public Mode SensorMode
        {
            get => ControlledSensor.SensorMode;
            set => ControlledSensor.SensorMode = value;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Update(Sensor sensor)
        {
            OnPropertyChanged(nameof(Value));
        }

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
