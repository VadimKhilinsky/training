﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using ApplicationModel.FileServices;
using Model;
using Model.FileManagement;

namespace ApplicationModel
{
    /// <summary>
    ///     for more information about organization of class
    ///     https://www.technical-recipes.com/2017/how-to-use-interaction-triggers-to-handle-user-initiated-events-in-wpf-mvvm/
    /// </summary>
    public class AppViewModel : INotifyPropertyChanged
    {
        private readonly DefaultFileService _defaultFileService = new DefaultFileService();
        private readonly object _obj = new object();
        private SensorCommand _add;
        private SensorCommand _changeMode;
        private SensorCommand _open;
        private SensorCommand _remove;
        private SensorCommand _save;
        private SensorController _selected;
        private string _selectedItem;

        public AppViewModel()
        {
            SensorControllers = new ObservableCollection<SensorController>();
            Items = new ObservableCollection<string>();
            foreach (SensorType e in Enum.GetValues(typeof(SensorType)))
            {
                Items.Add(e.ToString());
            }
        }

        public ObservableCollection<string> Items { get; set; }

        public ObservableCollection<SensorController> SensorControllers { get; set; }

        public string SelectedItem
        {
            get =>_selectedItem;
            set
            {
                _selectedItem = value;
                Selected.Type = (SensorType)Enum.Parse(typeof(SensorType), _selectedItem);
                OnPropertyChanged(nameof(SelectedItem));
            }
        } 

        public SensorController Selected
        {
            get => _selected;
            set
            {
                _selected = value;
                OnPropertyChanged(nameof(Selected));
            }
        }

        public SensorCommand Save
        {
            get
            {
                return _save ?? (_save = new SensorCommand(obj =>
                {
                    Storage storage = null;

                    try
                    {
                        _defaultFileService.SaveFileDialog();

                        if (_defaultFileService.Path.Contains(".xml")) storage = new XmlStorage();

                        if (_defaultFileService.Path.Contains(".json")) storage = new JsonStorage();

                        var sensorsSave = new List<Sensor>();

                        foreach (var sensorController in SensorControllers)
                            sensorsSave.Add(sensorController.ControlledSensor);

                        storage?.CreateFileManager().Save(_defaultFileService.Path, sensorsSave);
                        MessageBox.Show("File was successfully saved!");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }));
            }
        }

        public SensorCommand Open
        {
            get
            {
                return _open ?? (_open = new SensorCommand(obj =>
                {
                    Storage storage = null;

                    try
                    {
                        _defaultFileService.OpenFileDialog();

                        if (_defaultFileService.Path.Contains(".xml")) storage = new XmlStorage();

                        if (_defaultFileService.Path.Contains(".json")) storage = new JsonStorage();

                        var sensors = storage?.CreateFileManager().Read(_defaultFileService.Path);

                        SensorControllers.Clear();

                        foreach (var sensor in sensors) SensorControllers.Add(new SensorController(sensor));

                        MessageBox.Show("File was successfully opened!");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }));
            }
        }

        public SensorCommand Add => _add ?? (_add = new SensorCommand(obj =>
        {
            var sensorController = new SensorController(new Sensor());
            SensorControllers.Insert(0, sensorController);
            Selected = sensorController;
        }));

        public SensorCommand Remove => _remove ?? (_remove = new SensorCommand(obj =>
        {
            if (obj is SensorController sensor)
            {
                SensorControllers.Remove(sensor);
            }
        }, obj => SensorControllers.Count > 0));

        public SensorCommand ChangeMode
        {
            get
            {
                lock (_obj)
                {
                    return _changeMode ?? (_changeMode = new SensorCommand(obj =>
                    {
                        var sensor = obj as SensorController;

                        Task.Factory.StartNew(() => sensor?.ControlledSensor.SwitchMode());
                    }));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}