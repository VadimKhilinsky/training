﻿using System;

namespace Task1_2
{
    /// <summary>
    ///     Represents class of square matrices that all elements which are off diagonal are zero
    /// </summary>
    /// <typeparam name="T">Type of matrix elements</typeparam>
    public class DiagonalMatrix<T> : SquareMatrix<T>
    {
        public DiagonalMatrix(int m):base()
        {
            if (IsSizePositive(m))
            {
                Data = new T[m];
            }
        }

        public override int Size => Data.Length;

        protected override int Index(int i, int j)
        {
            return i;
        }

        protected override T GetData(int i, int j)
        {
            return i != j ? default : Data[Index(i, j)];
        }
    }
}