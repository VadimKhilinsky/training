﻿using System;

namespace Task1_2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var squareMatrix = new SquareMatrix<int>(4)
                {
                    [0, 0] = 3, [0, 1] = 4, [0, 2] = 5, [0, 3] = 4, [1, 0] = 3, [1, 1] = 5, [1, 2] = 5, [1, 3] = 5,
                    [2, 0] = 4, [2, 1] = 4, [2, 2] = 5, [2, 3] = 5, [3, 0] = 54, [3, 1] = 4, [3, 2] = 4, [3, 3] = 675
                };

                Console.WriteLine($"Square matrix: \n{squareMatrix}");

                var diagonalMatrix = new DiagonalMatrix<int>(5)
                    {[0, 0] = 2, [1, 1] = 3, [2, 2] = 5, [3, 3] = 7, [4, 4] = 12};

                Console.WriteLine($"Diagonal matrix: \n{diagonalMatrix}");

                diagonalMatrix.ElementChanged += DisplayChanges;

                diagonalMatrix[0, 1] = 2;
                diagonalMatrix[4, 4] = 1;

                diagonalMatrix.ElementChanged += (obj, argsChanged) =>
                    Console.WriteLine(
                        $"Element of diagonal matrix with indices [{argsChanged.I},{argsChanged.J}] was changed");

                diagonalMatrix.ElementChanged -= DisplayChanges;

                diagonalMatrix[2, 2] = 2;

                Console.WriteLine(diagonalMatrix);

                squareMatrix.ElementChanged += delegate(object sender, ElementChangedEventArgs<int> argsChanged)
                {
                    Console.WriteLine(
                        $"Element of square matrix with indices [{argsChanged.I},{argsChanged.J}] has changed value from {argsChanged.OldValue} to {argsChanged.NewValue}");
                };

                squareMatrix[2, 3] = 12;

                Console.WriteLine(squareMatrix);
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Program finished");
                Console.ReadLine();
            }
        }

        public static void DisplayChanges<T>(object sender, ElementChangedEventArgs<T> args)
        {
            Console.WriteLine($"matrix[{args.I}][{args.J}]: {args.OldValue} -> {args.NewValue} \n");
        }
    }
}