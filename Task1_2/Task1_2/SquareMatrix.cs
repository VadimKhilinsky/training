﻿using System;
using System.Configuration;
using System.Text;

namespace Task1_2
{
    /// <summary>
    ///     Represents class of matrices that have similar number of rows and columns
    /// </summary>
    /// <typeparam name="T">Type of matrix elements</typeparam>
    public class SquareMatrix<T>
    {
        /// <summary>
        ///     Represents array of elements of matrix
        /// </summary>
        private protected T[] Data;

        /// <summary>
        ///     Constructor with 1 parameter
        /// </summary>
        /// <param name="m">Size of matrix</param>
        public SquareMatrix(int m)
        {
            if (IsSizePositive(m))
            {
                Data = new T[m * m];
            }
        }

        /// <summary>
        ///     Default constructor
        /// </summary>
        private protected SquareMatrix()
        {
        }

        /// <summary>
        ///     Check if the input of matrix size is correct
        /// </summary>
        /// <param name="m">size of matrix</param>
        /// <returns>true if correct else false</returns>
        protected bool IsSizePositive(int m)
        {
            return m >= 0 ? true : throw new ArgumentException("size of matrix should be positive");
        }

        /// <value>
        ///     Represents size of square matrix
        /// </value>
        public virtual int Size => (int) Math.Sqrt(Data.Length);

        /// <summary>
        ///     Indexer with 2 params
        /// </summary>
        /// <param name="i">number of row</param>
        /// <param name="j">number of column</param>
        /// <returns>element of matrix that is on i row and j column</returns>
        public T this[int i, int j]
        {
            get
            {
                CheckIndexes(i, j);

                return GetData(i,j);
            }
            set
            {
                CheckIndexes(i, j);

                if (TrySetNewValue(Index(i,j), value, out var oldValue))
                {
                    OnElementChanged(new ElementChangedEventArgs<T>(i, j, oldValue, value));
                }
            }
        }

        /// <summary>
        /// Transform index to linear
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <returns>linear index</returns>
        protected virtual int Index(int i, int j)
        {
            return i * Size + j;
        }

        /// <summary>
        /// Get matrix element by indexes
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <returns></returns>
        protected virtual T GetData(int i, int j)
        {
            return Data[Index(i,j)];
        }

        /// <summary>
        ///     Make a try to set a value of matrix by index
        /// </summary>
        /// <param name="index">linear index of matrix</param>
        /// <param name="value">new value to set</param>
        /// <param name="oldValue">value that is in matrix by index</param>
        /// <returns></returns>
        protected bool TrySetNewValue(int index, T value, out T oldValue)
        {
            oldValue = Data[index];

            if (!Equals(oldValue, value))
            {
                Data[index] = value;

                return true;
            }

            return false;
        }

        /// <summary>
        ///     Represents event which occurs after matrix element has changed
        ///     <see cref="ElementChangedEventArgs{T}" />
        /// </summary>
        public event EventHandler<ElementChangedEventArgs<T>> ElementChanged;


        /// <summary>
        ///     Generates event
        /// </summary>
        /// <param name="value">args of event</param>
        protected virtual void OnElementChanged(ElementChangedEventArgs<T> value)
        {
            ElementChanged?.Invoke(this, value);
        }

        /// <summary>
        ///     Check if indeces satisfy the size of matrix
        /// </summary>
        /// <param name="i">number of row</param>
        /// <param name="j">number of column</param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        protected void CheckIndexes(int i, int j)
        {
            if (i < 0 || i >= Size) throw new IndexOutOfRangeException(nameof(i));

            if (j < 0 || j >= Size) throw new IndexOutOfRangeException(nameof(j));
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++) sb.Append($"{this[i, j]}  ");
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
