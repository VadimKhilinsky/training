﻿using System;

namespace Task1_2
{
    /// <summary>
    ///     Event arguments of event delegate
    /// </summary>
    /// <typeparam name="T">Type of delegate elements</typeparam>
    public class ElementChangedEventArgs<T> : EventArgs
    {
        public ElementChangedEventArgs(int i, int j, T oldValue, T newValue)
        {
            I = i;
            J = j;
            OldValue = oldValue;
            NewValue = newValue;
        }

        /// <summary>
        ///     number of row
        /// </summary>
        public int I { get; }

        /// <summary>
        ///     number of column
        /// </summary>
        public int J { get; }

        /// <summary>
        ///     value before change
        /// </summary>
        public T OldValue { get; }

        /// <summary>
        ///     value after change
        /// </summary>
        public T NewValue { get; }
    }
}