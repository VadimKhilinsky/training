﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Task2_2
{
    public class Login
    {
        public string Title { get; }
        public List<Window> Windows { get; }
        public Login(string title, params Window[] windows)
        {
            Title = title;
            Windows = new List<Window>();

            if (windows == null)
            {
                return;
            }

            foreach (var window in windows)
            {
                Windows.Add(window);
            }
        }

        public void CorrectConfiguration()
        {
            foreach (var window in Windows)
            {
                window.CorrectValues();
            }
        }

        public bool IsConfigurationCorrect()
        {
            foreach (var window in Windows)
            {
                if (!window.IsCorrect())
                {
                    return false;
                }
            }

            return true;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Login: {Title}");

            foreach (var window in Windows)
            {
                sb.AppendLine(window.ToString());
            }

            return sb.ToString();
        }
    }
}
