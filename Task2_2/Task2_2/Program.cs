﻿using System;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;

namespace Task2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var loginReader = new LoginXmlReader();
            var i = 2;
            var directory = GetProjectDirectory(Environment.CurrentDirectory, ref i);
            var config = loginReader.ReadLogins(directory + @"\Config\logins.xml");

            Console.WriteLine(config);

            var incorrectLogins = config.GetIncorrectLogins();

            Console.WriteLine("Incorrect logins:");
            foreach (var incorrectLogin in incorrectLogins)
            {
                Console.WriteLine(incorrectLogin);
            }

            foreach (var login in config.Logins)
            {
                login.CorrectConfiguration();
                using (StreamWriter sw = new StreamWriter(File.Create(directory + $@"\Config\{login.Title}.json")))
                {
                    string jsonString = JsonConvert.SerializeObject(login, Formatting.Indented);
                    sw.Write(jsonString);
                }
            }

            Console.ReadLine();
        }

        private static string GetProjectDirectory(string path, ref int i)
        {
            if (i == 0) return path;
            i--;
            return GetProjectDirectory(Directory.GetParent(path).FullName, ref i);
        }
    }
}