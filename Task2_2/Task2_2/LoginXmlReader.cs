﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.Linq;
using System.Security.Principal;


namespace Task2_2
{
    public class LoginXmlReader
    {
        public Config ReadLogins(string path)
        {
            var xml = XDocument.Load(path);

            if (xml == null)
            {
                throw new ArgumentException("path cannot be found");
            }

            var logins = from login in xml.Descendants("login")
                select new
                {
                    Title = login.Attribute("name")?.Value,
                    Windows = from window in login.Elements("window")
                        select new
                        {
                            Title = window.Attribute("title")?.Value,
                            Top = window.Element("top")?.Value,
                            Left = window.Element("left")?.Value,
                            Width = window.Element("width")?.Value,
                            Height = window.Element("height")?.Value
                        }
                };


            var listLogins = new List<Login>();

            foreach (var loginData in logins)
            {
                var login = new Login(loginData.Title, null);

                foreach (var window in loginData.Windows)
                {
                    login.Windows.Add(new Window(window.Title,
                        window.Top, window.Left,
                        window.Width, window.Height));
                }

                listLogins.Add(login);
            }
            var config = new Config
            {
                Logins = listLogins
            };

            return config;
        }

        

}
}
