﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_2
{
    public class Config
    {
        public List<Login> Logins { get; set; }

        public Config()
        {
            Logins = new List<Login>();
        }

        public IEnumerable<Login> GetIncorrectLogins()
        {
            Console.WriteLine();
            foreach (var login in Logins)
            {
                if (!login.IsConfigurationCorrect())
                {
                    yield return login;
                }
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            foreach (var login in Logins)
            {
                sb.AppendLine(login.ToString());
            }

            return sb.ToString();
        }
    }
}

