﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Task2_2
{
    public class Window
    {
        public string Title { get; set; }
        public int? Top { get; set; }
        public int? Left { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }

        public Window(string title, string top, string left, string width, string height)
        {
            Title = title;
            Top = Parse(top);
            Left = Parse(left);
            Width = Parse(width);
            Height = Parse(height);
        }

        public override string ToString()
        {
            return new StringBuilder().Append($"{Title}(" +
                                              $"{CoordinateStringValue(Top)}," +
                                              $"{CoordinateStringValue(Left)}," +
                                              $"{CoordinateStringValue(Width)}," +
                                              $"{CoordinateStringValue(Height)}" +
                                              ")").ToString();
        }

        private string CoordinateStringValue(int? coordinate)
        {
            return coordinate == null ? "?" : coordinate.ToString();
        }

        public bool IsCorrect()
        {
            if (Title == "main")
            {
                return !(Top == null || Left == null || Width == null || Height == null);
            }

            return true;
        }

        public void CorrectValues()
        {
            Top = Top ?? 0;
            Left = Left ?? 0;
            Width = Width ?? 400;
            Height = Height ?? 150;
        }

        private int? Parse(string value)
        {
            if (value == null)
            {
                return null;
            }

            return int.Parse(value);
        }
    }
}
