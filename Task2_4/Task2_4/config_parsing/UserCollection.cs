﻿using System.Configuration;

namespace Task2_4.config_parsing
{
    [ConfigurationCollection(typeof(SiteElement), AddItemName = "user")]
    public class UserCollection : ConfigurationElementCollection
    {
        public UserElement this[int i]
        {
            get => (UserElement)BaseGet(i);
            set
            {
                if (BaseGet(i) != null)
                {
                    BaseRemoveAt(i);
                }
                BaseAdd(i, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new UserElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UserElement)element).Email;
        }
    }
}