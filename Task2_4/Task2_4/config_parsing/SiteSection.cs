﻿using System.Configuration;

namespace Task2_4.config_parsing
{
    public class SiteSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsDefaultCollection = true)]
        public SiteCollection Sites => (SiteCollection) base[""];
    }
}