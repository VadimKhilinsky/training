﻿using System.Configuration;

namespace Task2_4.config_parsing
{
    public class UserSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsDefaultCollection = true)]
        public UserCollection Users => (UserCollection)base[""];
    }
}