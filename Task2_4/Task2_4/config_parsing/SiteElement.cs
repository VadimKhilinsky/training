﻿using System.Configuration;

namespace Task2_4.config_parsing
{
    public class SiteElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string SiteName
        {
            get => (string) this["name"];
            set => this["name"] = value;
        }

        [ConfigurationProperty("checkinterval", IsRequired = true)]
        public float CheckInterval
        {
            get => (float) this["checkinterval"];
            set => this["checkinterval"] = value;
        }

        [ConfigurationProperty("maxresponse", IsRequired = true)]
        public float MaxResponse
        {
            get => (float) this["maxresponse"];
            set => this["maxresponse"] = value;
        }

        [ConfigurationProperty("email", IsRequired = true)]
        public string Email
        {
            get => (string) this["email"];
            set => this["email"] = value;
        }
    }
}