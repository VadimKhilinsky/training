﻿using System.Configuration;

namespace Task2_4.config_parsing
{
    [ConfigurationCollection(typeof(SiteElement), AddItemName = "site")]
    public class SiteCollection : ConfigurationElementCollection
    {
        public SiteElement this[int i]
        {
            get=> (SiteElement)BaseGet(i);
            set
            {
                if (BaseGet(i) != null)
                {
                    BaseRemoveAt(i);
                }
                BaseAdd(i,value);
            }
        } 

        protected override ConfigurationElement CreateNewElement()
        {
            return new SiteElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SiteElement)element).SiteName;
        }
    }
}
