﻿using System.Configuration;

namespace Task2_4.config_parsing
{
    public class UserElement : ConfigurationElement
    {
        [ConfigurationProperty("email", IsRequired = true)]
        public string Email
        {
            get => (string)this["email"];
            set => this["email"] = value;
        }

        [ConfigurationProperty("password", IsRequired = true)]
        public string Password
        {
            get => (string)this["password"];
            set => this["password"] = value;
        }

        
    }
}