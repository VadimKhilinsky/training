﻿using System.Collections.Generic;
using System.Configuration;
using Task2_4.config_parsing;

namespace Task2_4
{
    public static class UsersReader
    {
        public static List<(string,string)> Read()
        {
            ConfigurationManager.RefreshSection("users");

            var userSection = (UserSection)ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
                .GetSection("users");

            var users = new List<(string,string)>();
            var numUsers = userSection.Users.Count;

            for (var i = 0; i < numUsers; i++)
            {
                users.Add(GetUserByElement(userSection.Users[i]));
            }

            return users;
        }

        private static (string,string) GetUserByElement(UserElement element)
        {
            return (element.Email, element.Password);
        }
    }
}