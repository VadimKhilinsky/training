﻿using System.Collections.Generic;
using System.Configuration;
using Task2_4.config_parsing;

namespace Task2_4
{
    public static class SiteReader
    {
        public static List<Site> Read()
        {
            ConfigurationManager.RefreshSection("sites");

            var siteSection = (SiteSection) ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
                .GetSection("sites");

            var sites = new List<Site>();
            var numSites = siteSection.Sites.Count;

            for (var i = 0; i < numSites; i++)
            {
                sites.Add(GetSiteByElement(siteSection.Sites[i]));
            }

            return sites;
        }

        private static Site GetSiteByElement(SiteElement element)
        {
            return new Site(element.SiteName, element.CheckInterval, element.Email, element.MaxResponse);
        }
    }
}