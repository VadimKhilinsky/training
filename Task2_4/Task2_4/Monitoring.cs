﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Task2_4
{
    public class Monitoring : IDisposable
    {
        private string _pathToConfigs;
        private List<Site> _sites;
        private FileSystemWatcher _watcher;

        public Monitoring(string pathConfigs)
        {
            CheckNullOrEmpty(pathConfigs);

            _pathToConfigs = pathConfigs;

            StartMonitoring();
        }

        private void StartMonitoring()
        {
            _sites = SiteReader.Read();

            Update();

            var configFileName = Path.GetFileName(_pathToConfigs);

            var configFilePath = _pathToConfigs?.Substring(0,
                _pathToConfigs.IndexOf(configFileName ?? throw new InvalidOperationException(),
                    StringComparison.Ordinal));

            _watcher = new FileSystemWatcher(configFilePath ?? throw new InvalidOperationException(), configFileName)
            {
                NotifyFilter = NotifyFilters.FileName | NotifyFilters.LastWrite | NotifyFilters.LastAccess
            };

            AddHandler();
        }

        private void Update()
        {

            _sites = SiteReader.Read();

            foreach (var site in _sites)
            {
                site.StartTesting();
            }
        }

        private void AddHandler()
        {
            var handler = new FileSystemEventHandler((o, e) => Update());
            _watcher.Changed += handler;
            _watcher.Created += handler;
            _watcher.Deleted += handler;
            _watcher.Renamed += (o, e) => _pathToConfigs = e.FullPath;
            _watcher.EnableRaisingEvents = true;
        }

        private void CheckNullOrEmpty(string str)
        {
            if (string.IsNullOrEmpty(str)) throw new ArgumentNullException(nameof(str));
        }

        public void Dispose()
        {
            foreach (var site in _sites)
            {
                site.Dispose();
            }
        }
    }
}