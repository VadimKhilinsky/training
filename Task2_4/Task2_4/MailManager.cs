﻿using System;
using System.Net;
using System.Net.Mail;
using NLog;

namespace Task2_4
{
    public class MailManager : IDisposable
    {
        private static MailManager _instance;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly SmtpClient _client;
        private readonly MailAddress _from;
        private MailManager()
        {
            (string, string) user = UsersReader.Read()[0];
            _client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential(user.Item1, user.Item2),
                EnableSsl = true,
            };
            _from = new MailAddress(user.Item1,"SomeName");
        }

        public static MailManager GetInstance()
        {
            return _instance ?? (_instance = new MailManager());
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public async void SendMessageToAdminAsync(string adminEmail, string message)
        {
            var to = new MailAddress(adminEmail);

            var mailMessage = new MailMessage(_from, to)
            {
                Subject = $"Your site is unavailable",
                Body = $"Attempt to make request to site was unsuccessful {message}"
            };

            try
            {
                await _client.SendMailAsync(mailMessage);
            }
            catch (SmtpException ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);
            }
        }
    }
}