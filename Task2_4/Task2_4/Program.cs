﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using NLog;

namespace Task2_4
{
    public class Program
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public static string rootFolderPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        public static string configFilePath = Path.Combine(rootFolderPath, "Task2_4.exe.config");

        private static void Main(string[] args)
        {
            using (var mutex = new Mutex(false, "MainMutex"))
            {
                if (!mutex.WaitOne(TimeSpan.FromSeconds(3), false))
                {
                    Console.WriteLine("Another instance is running");

                    return;
                }

                Run();
            }
        }

        private static void Run()
        {
            var monitoring = new Monitoring($"{configFilePath}");
            Console.WriteLine("Finished");
            Console.ReadLine();
        }
    }
}