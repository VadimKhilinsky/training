﻿using System;
using System.Net.NetworkInformation;
using System.Threading;
using NLog;

namespace Task2_4
{
    public class Site : IDisposable
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private string _address;
        private float _checkInterval;
        private string _email;
        private float _maxResponseTime;
        private Timer _checkingTimer;

        public Site(string address, float checkInterval, string email, float maxResponseTime)
        {
            Address = address;
            CheckInterval = checkInterval;
            Email = email;
            MaxResponseTime = maxResponseTime;
        }

        public float CheckInterval
        {
            get => _checkInterval;
            set => _checkInterval = value >= 0 ? value : throw new ArgumentException(nameof(value));
        }

        public float MaxResponseTime
        {
            get => _maxResponseTime;
            set => _maxResponseTime = value >= 0 ? value : throw new ArgumentException(nameof(value));
        }

        public string Address
        {
            get => _address;
            set => _address = string.IsNullOrEmpty(value) ? throw new ArgumentNullException(nameof(value)) : value;
        }

        public string Email
        {
            get => _email;
            set => _email = string.IsNullOrEmpty(value) ? throw new ArgumentNullException(nameof(value)) : value;
        }

        public void Dispose()
        {
            _checkingTimer.Dispose();
        }

        public void StartTesting()
        {
            if (_checkingTimer == null)
            {
                _checkingTimer = new Timer(obj => Test(),this,10, (long)(1000 * CheckInterval));
            }
            else
            {
                _checkingTimer.Change((long)(1000 * CheckInterval), (long)(1000 * CheckInterval));
            }
        }

        private void Test()
        {
            
            var ping = new Ping();
            try
            {
                var pingReply = ping.Send(Address, (int) (MaxResponseTime * 1000));

                if (pingReply == null)
                { 
                    MailManager.GetInstance()
                        .SendMessageToAdminAsync(Email, $"({Address}) due to site doesn't exist.");
                }
                else if (pingReply.Status == IPStatus.TimedOut)
                {
                    MailManager.GetInstance().SendMessageToAdminAsync(Email, $"({Address}) due to time out.");
                }
                else
                {
                    _logger.Info(
                        $"request to {Address} was {pingReply.Status}, trip time = {pingReply.RoundtripTime}");
                }
            }
            catch (PingException ex)
            {
                MailManager.GetInstance().SendMessageToAdminAsync(Email, $"({Address}) due to site doesn't exist.");
            }
        }
    }
}