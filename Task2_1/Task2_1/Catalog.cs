﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Task2_1
{
    public class Catalog : IEnumerable<Book>
    {
        private readonly Dictionary<string, Book> _bookCatalog;

        public Catalog()
        {
            _bookCatalog = new Dictionary<string, Book>();
        }

        public Book this[string isbn] => _bookCatalog[IsbnChecker.Normalize(isbn)];

        public void Add(Book book)
        {
            _bookCatalog.Add(book.Isbn,book);
        }

        public IEnumerator<Book> GetEnumerator()
        {
            return _bookCatalog.Values.OrderBy(book => book.Title).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerable<Book> GetBooksByAuthor(string firstName, string lastName)
        {
            var author = new Author(firstName, lastName);
            return _bookCatalog.Values.Where(book => book.GetAuthors().Contains(author));
        }

        public IEnumerable<Book> GetBooksSortedByDescendingPublicationDate()
        {
            return _bookCatalog.Values.OrderByDescending(book => book.PublicationDate);
        }

        public IEnumerable<(Author, int)> GetAuthorBookCount()
        {
            return from book in _bookCatalog.Values
                from author in book.GetAuthors()
                group book by author
                into pair
                select (pair.Key, pair.Count());
        }
    }
}