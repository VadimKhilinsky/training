﻿using System;
using System.Collections.Generic;

namespace Task2_1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var author1 = new Author("dklsjf", "dshfu");
                var author2 = new Author("okdfslg", "sdfjsdl");
                var author3 = new Author("hsfkljdh", "hgfghus");
                var author4 = new Author("pokdfpo", "rtgdf");
                var author5 = new Author("bnxbmsnd", "ewutdfs");
                var author6 = new Author("hdkbvx", "yifvsd");

                var book1 = new Book("123-4-56-789012-3","Gjhsdfgj", new DateTime(2000, 11, 20), new List<Author>(){author1, author2});
                var book2 = new Book("1245769753951","LIsdfljd", new DateTime(2001, 10, 11), new List<Author>(){author1, author3, author4});
                var book3 = new Book("234-4-65-947694-3", "Ekjdhsfk", new DateTime(2010, 3, 10));
                var book4 = new Book("7557874675679","Asfjlsdfj", new DateTime(1999, 4, 19), new List<Author>(){author5, author6});
                var book5 = new Book("755-8-56-345667-3","Bkhsfdkjhdf");

                var catalog = new Catalog();
                catalog.Add(book1);
                catalog.Add(book2);
                catalog.Add(book3);
                catalog.Add(book4);
                catalog.Add(book5);

                foreach (var book in catalog)
                {
                    Console.WriteLine(book);
                }

                Console.WriteLine(new string('-', 20));

                var selected = catalog.GetBooksByAuthor("dklsjf", "dshfu");
                Console.WriteLine("Founded book: \n");

                foreach (var book in selected)
                {
                    Console.WriteLine(book);
                }

                Console.WriteLine(new string('-', 20));

                var descPublicationDateBooks = catalog.GetBooksSortedByDescendingPublicationDate();
                Console.WriteLine("Books sorted by descending publication date: \n");

                foreach (var book in descPublicationDateBooks)
                {
                    Console.WriteLine(book);
                }

                Console.WriteLine(new string('-', 20));

                var authorsBookCount = catalog.GetAuthorBookCount();
                Console.WriteLine("Number of books for each author: \n");

                foreach (var authorBookCount in authorsBookCount)
                {
                    Console.WriteLine($"{authorBookCount.Item1}\nBook count: {authorBookCount.Item2}\n");
                }

                Console.WriteLine(new string('-', 20));
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
