﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2_1
{
    public sealed class Book
    {
        private readonly HashSet<Author> _authors = new HashSet<Author>();
        public string Isbn { get; }
        public string Title { get; }
        public DateTime? PublicationDate { get; }

        public Book(string isbn, string title, DateTime? publicationDate = null, IEnumerable<Author> authors = null)
        {
            IsbnChecker.Check(isbn);
            Isbn = IsbnChecker.Normalize(isbn);
            CheckTitle(title);
            Title = title;
            PublicationDate = publicationDate;

            if (authors != null)
            {
                foreach (var author in authors)
                {
                    if (author != null)
                    {
                        _authors.Add(author);
                    }
                }
            }
        }

        private void CheckTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException("Must be non-empty", nameof(title));
            }

            if (title.Length > 1000)
            {
                throw new ArgumentException("Length must be not more than 1000");
            }
        }

        public IEnumerable<Author> GetAuthors() => _authors;

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("Title: ").AppendLine(Title);
            sb.Append("Publication date: ").AppendLine(PublicationDate.ToString());
            if (_authors.Count > 0)
            {
                int i = 1;
                sb.AppendLine("Author(s):");
                foreach (var author in _authors)
                {
                    sb.Append($"{i})").AppendLine(author.ToString());
                    i++;
                }
            }

            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            var book = obj as Book;

            if (book == null)
            {
                return false;
            }

            return Isbn == book.Isbn;
        }

        public override int GetHashCode()
        {
            return Isbn.GetHashCode();
        }
    }
}
