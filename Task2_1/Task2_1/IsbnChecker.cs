﻿using System;
using System.Text.RegularExpressions;

namespace Task2_1
{
    public static class IsbnChecker
    {
        private const string IsbnPattern = @"^\d{13}$|^\d{3}-\d{1}-\d{2}-\d{6}-\d{1}$";
        private static readonly Regex IsbnRegex = new Regex(IsbnPattern);

        public static void Check(string isbn)
        {
            if (string.IsNullOrEmpty(isbn))
            {
                throw new ArgumentException("Must be non-empty", nameof(isbn));
            }

            if (!IsbnRegex.IsMatch(isbn))
            {
                throw new ArgumentException(
                    $"Format of {nameof(isbn)} must satisfy 'DDD-D-DD-DDDDDD-D' or 'DDDDDDDDDDDDD' where D is 0..9");
            }
        }

        public static string Normalize(string isbn)
        {
            return isbn.Replace("-", "");
        }
    }
}
