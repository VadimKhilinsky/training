﻿using System;

namespace Task2_1
{
    public class Author
    {
        public string FirstName { get; }
        public string LastName { get; }

        public Author(string firstName, string lastName)
        {
            CheckCorrect(firstName);
            CheckCorrect(lastName);

            FirstName = firstName.ToUpper();
            LastName = lastName.ToUpper();
        }

        private void CheckCorrect(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (str.Length > 200)
            {
                throw new ArgumentException("length of str should be not more 200", nameof(str));
            }
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }

            var author = obj as Author;

            if (author == null)
            {
                return false;
            }

            return FirstName.Equals(author.FirstName) && LastName.Equals(author.LastName);
        }

        public override int GetHashCode()
        {
            return (FirstName+LastName).GetHashCode();
        }

        public override string ToString()
        {
            return $"First name: {FirstName} Last name: {LastName}";
        }
    }
}