﻿using System.Collections.Generic;
using NUnit.Framework;

namespace Task2_1.Tests
{
    [TestFixture]
    public class IsbnTests
    {

        private static readonly List<TestCaseData> TestsImplicit = new List<TestCaseData>
        {
            new TestCaseData(null, true),
            new TestCaseData("123456789012",true),
            new TestCaseData("123-2-34-456787",true),
            new TestCaseData("123-4-56-789012-3",false),
            new TestCaseData("1234567890123", false)
        };


        [Test]
        [TestCaseSource(nameof(TestsImplicit))]
        public void Implicit_Operator_Test(string testString, bool isThrowsException)
        {
            if (isThrowsException)
            {
                Assert.That(() => IsbnChecker.Check(testString), Throws.ArgumentException);
            }
            else
            {
                Assert.That(() => IsbnChecker.Check(testString), Throws.Nothing);
            }
        }


    }
}