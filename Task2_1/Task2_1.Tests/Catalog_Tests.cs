﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Task2_1.Tests
{
    [TestFixture]
    public class CatalogTests
    {

        private static readonly Author[] AuthorDataSet = new[]
        {
            new Author("dklsjf", "dshfu"),
            new Author("okdfslg", "sdfjsdl"),
            new Author("hsfkljdh", "hgfghus"),
            new Author("pokdfpo", "rtgdf"),
            new Author("bnxbmsnd", "ewutdfs"),
            new Author("hdkbvx", "yifvsd")
        };

        private static readonly Book[] BookDataSet = new[]
        {
            new Book("123-4-56-789012-3","Gjhsdfgj", new DateTime(2000, 11, 20), new List<Author>(){AuthorDataSet[0], AuthorDataSet[1]}),
            new Book("1245769753951","LIsdfljd", new DateTime(2001, 10, 11), new List<Author>(){AuthorDataSet[0], AuthorDataSet[2], AuthorDataSet[3]}),
            new Book("234-4-65-947694-3","Ekjdhsfk", new DateTime(2010, 3, 10), new List<Author>(){AuthorDataSet[1]}),
            new Book("7557874675679","Asfjlsdfj", new DateTime(1999, 4, 19), new List<Author>(){AuthorDataSet[4], AuthorDataSet[5]}),
            new Book("755-8-56-345667-3","Bkhsfdkjhdf", new DateTime(2004, 12, 3), new List<Author>(){AuthorDataSet[0], AuthorDataSet[2], AuthorDataSet[4]})
        };

        private static readonly Catalog[] CatalogDataSet = new[]
        {
            new Catalog()
            {
                BookDataSet[0],
                BookDataSet[1],
                BookDataSet[2],
                BookDataSet[3],
                BookDataSet[4]
            },
            new Catalog()
            {
                 BookDataSet[2],
                 BookDataSet[4]
            }
        };

        private static readonly List<TestCaseData> TestsCatalogFindingBooks = new List<TestCaseData>
        {
            new TestCaseData(
                CatalogDataSet[0], "dklsjf", "dshfu", 3),
            new TestCaseData(CatalogDataSet[0],"pokdfpo", "rtgdf", 1)
        };

        private static readonly List<TestCaseData> TestsAuthorBooksSize = new List<TestCaseData>
        {
            new TestCaseData(
                CatalogDataSet[0], new int[]{3,2,2,1,2,1}),
            new TestCaseData(CatalogDataSet[1], new int[]{1,1,1,1})
        };

        [Test]
        [TestCaseSource(nameof(TestsCatalogFindingBooks))]
        public void Find_books_by_author(Catalog catalog, string firstname, string lastname, int resultSize)
        {
            Assert.AreEqual(catalog.GetBooksByAuthor(firstname,lastname).ToList().Count,resultSize);
        }

        [Test]
        [TestCaseSource(nameof(TestsAuthorBooksSize))]
        public void Authors_book_size(Catalog catalog, int[] resultSize)
        {
            var authorsBookCount = catalog.GetAuthorBookCount();
            var i = 0;
            foreach (var authorBookCount in authorsBookCount)
            {
                Assert.AreEqual(authorBookCount.Item2, resultSize[i]);
                ++i;
            }
        }






    }
}