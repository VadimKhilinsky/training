﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Task2_1.Tests
{
    [TestFixture]
    public class BookTests
    {
        private static readonly string More1000 = new string('1', 1001);

        private static readonly List<TestCaseData> Tests = new List<TestCaseData>
        {
            new TestCaseData("1234567890123",More1000,null,null,null),
            new TestCaseData("123-3-23-456789-3","", new DateTime(1, 1, 1), new Author("dksfm", "sdfjsd"), new Author("dojdf", "oweo"))
        };

        private static IEnumerable<TestCaseData> TestCases()
        {
            foreach (var test in Tests)
            {
                yield return test;
            }
        }

        [Test]
        [TestCaseSource(nameof(TestCases))]
        public void Constructor_Test(string isbn, string title, DateTime publicationDate, Author author1, Author author2)
        {
            Assert.That(() => new Book(isbn, title, publicationDate, new List<Author>(){author1, author2}), Throws.ArgumentException);
        }
    }
}